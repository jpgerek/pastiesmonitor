package org.utad.tfm.crawlers

import scala.collection.mutable.Set
import scala.reflect.ClassTag

class CircularArray[T: ClassTag](capacity: Int) {
  assert(capacity > 0)

  var length = 0
  val storeArray = new Array[T](capacity)
  val storeSet = Set[T]()

  var currentOffset = 0
  var oldestOffset = -1

  def add(elem: T): Unit = {
    if (length < capacity) {
      length += 1
    } else {
      oldestOffset = (oldestOffset+1) % capacity
      storeSet.remove(storeArray(oldestOffset))
    }

    storeArray(currentOffset%capacity) = elem
    currentOffset = (currentOffset+1) % capacity

    storeSet.add(elem)
  }

  def contains(elem: T) = storeSet.contains(elem)
}
