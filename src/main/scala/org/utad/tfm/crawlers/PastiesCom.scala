package org.utad.tfm.crawlers

import java.util.Properties

import org.apache.kafka.clients.producer._
import org.json4s.NoTypeHints
import org.json4s.native.Serialization
import org.json4s.native.Serialization.write
import org.utad.tfm.common.{Config, Pastie}

import scala.util.Try



object PastiesCom {
  implicit val formats = Serialization.formats(NoTypeHints)

  val CLIENT_ID = "crawler-pasties.com"

  val maxPastiesExpectedPerPage = 100

  val recentIDs = new CircularArray[String](maxPastiesExpectedPerPage)

  val config = Config.get()
  println(config)

  val INTERVAL = 60 // seconds
  val RECENT_LIST_URL = "https://pastebin.com/archive"
  val PASTIE_RAW_BASE_URL = "https://pastebin.com/raw/"

  val KAFKA_APP_ID = "crawler.pastebin.com"

  val RECENT_PASTIE_REGEX = "<a href=\"/([^\"]{8})\">([^<]+)</a>".r

  def getRecentList():Iterator[Pastie] = {
    val result = Try(scala.io.Source.fromURL(RECENT_LIST_URL).mkString)
    if (result.isFailure) {
      System.err.println("Error getting url list")
      return List[Pastie]().toIterator
    }
    val content = result.get
    val preClean = Try(content.substring(content.indexOf("<table class=\"maintable\">"), content.indexOf("</table>"))).getOrElse("")
    return for (m <- RECENT_PASTIE_REGEX.findAllMatchIn(preClean)) yield Pastie(name = CLIENT_ID, id = m.group(1), title = m.group(2))
  }

  def getContent(id:String):String = {
    val result = Try(scala.io.Source.fromURL(PASTIE_RAW_BASE_URL + id).mkString)
    if (result.isFailure) {
      System.err.println("Error getting paste with id: " + id)
    }
    return result.getOrElse("")
  }

  def publish(producer: KafkaProducer[String, String], p: Pastie) = {
    val record = new ProducerRecord(config.topicPasties, KAFKA_APP_ID, write(p))
    producer.send(record)
  }

  def main(args: Array[String]) {
    val  props = new Properties()
    props.put("bootstrap.servers", config.kafkaBootstrapServer)
    props.put("client.id", CLIENT_ID)
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    while (true) {
      for (p <- getRecentList()) {
        if (!recentIDs.contains(p.id)) {
          print(".")
          recentIDs.add(p.id)
          val pWithContent = p.copy(content = getContent(p.id), url = PASTIE_RAW_BASE_URL + p.id)
          publish(producer, pWithContent)
        }
      }
      Thread.sleep(INTERVAL*1000)
      println("")
    }
  }
}
