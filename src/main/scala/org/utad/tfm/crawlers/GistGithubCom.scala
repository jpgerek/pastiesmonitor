package org.utad.tfm.crawlers

import java.util.Properties

import org.apache.kafka.clients.producer._
import org.json4s.NoTypeHints
import org.json4s.native.Serialization
import org.json4s.native.Serialization.write
import org.utad.tfm.common.{Config, Pastie}

import scala.util.Try



object GistGithubCom {
  implicit val formats = Serialization.formats(NoTypeHints)

  val CLIENT_ID = "crawler-gist.github.com"

  val maxPastiesExpectedPerPage = 50

  val recentIDs = new CircularArray[String](maxPastiesExpectedPerPage)

  val config = Config.get()
  println(config)

  val INTERVAL = 60 // seconds
  val RECENT_LIST_URL = "https://gist.github.com/discover"
  val PASTIE_RAW_BASE_URL = "https://gist.githubusercontent.com/raw/"

  val KAFKA_APP_ID = "crawler-gist.github.com"

  val RECENT_PASTIE_REGEX = "<a\\s+href=\"https:\\/\\/gist\\.github\\.com\\/[^\\/]+\\/([a-f0-9]{32})\"".r

  def getRecentList():Iterator[Pastie] = {
    val result = Try(scala.io.Source.fromURL(RECENT_LIST_URL).mkString)
    if (result.isFailure) {
      System.err.println("Error getting url list")
      return List[Pastie]().toIterator
    }
    val content = result.get
    return for (m <- RECENT_PASTIE_REGEX.findAllMatchIn(content)) yield Pastie(name = CLIENT_ID, id = m.group(1), title ="")
  }

  def getContent(id:String):String = {
    val result = Try(scala.io.Source.fromURL(PASTIE_RAW_BASE_URL + id).mkString)
    if (result.isFailure) {
      System.err.println("Error getting paste with id: " + id)
    }
    return result.getOrElse("")
  }

  def publish(producer: KafkaProducer[String, String], p: Pastie) = {
    val record = new ProducerRecord(config.topicPasties, KAFKA_APP_ID, write(p))
    producer.send(record)
  }

  def main(args: Array[String]) {
    val  props = new Properties()
    props.put("bootstrap.servers", config.kafkaBootstrapServer)
    props.put("client.id", CLIENT_ID)
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    while (true) {
      for (p <- getRecentList()) {
        if (!recentIDs.contains(p.id)) {
          print(".")
          recentIDs.add(p.id)
          val pWithContent = p.copy(content = getContent(p.id), url = PASTIE_RAW_BASE_URL + p.id)
          publish(producer, pWithContent)
        }
      }
      Thread.sleep(INTERVAL*1000)
      println("")
    }
  }
}
