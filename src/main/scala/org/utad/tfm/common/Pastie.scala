package org.utad.tfm.common

import java.sql.Timestamp

import org.json4s.native.Serialization
import org.json4s.{Formats, NoTypeHints}
import org.json4s.native.Serialization.{read, write}
import org.utad.tfm.crawlers.PastiesCom

import scala.text

case class Pastie(name:String, id:String, content:String = "-", site:String = "-", url:String = "-", timestamp:Timestamp = new java.sql.Timestamp((new java.util.Date).getTime), title:String = "-")

object Pastie {
  implicit val format = Serialization.formats(NoTypeHints)

  def parse(raw:String):Pastie = return read[Pastie](raw)
}