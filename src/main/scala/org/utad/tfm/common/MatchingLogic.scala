package org.utad.tfm.common

import org.json4s._
import org.json4s.native.Serialization
import org.json4s.native.Serialization.read

import scala.util.matching.Regex

case class RegexCountPre(regex:String, count:Int)

private case class MatchingLogicPre(id:String, matchingRules:List[RegexCountPre], excludingRules:List[RegexCountPre]) {

  implicit val formats = Serialization.formats(NoTypeHints)

  def get(): MatchingLogic = {
    return MatchingLogic(id,
      matchingRules.map((t:RegexCountPre) => RegexCount(t.regex.r, t.count)),
      excludingRules.map((t:RegexCountPre) => RegexCount(t.regex.r, t.count)))
  }
}

case class RegexCount(regex:Regex, count:Int)
case class MatchingLogic(id:String, matchingRules:List[RegexCount], excludingRules:List[RegexCount]) {
  def matches(p: Pastie): Boolean = {
    for (t <- excludingRules) {
      if (t.regex.findAllIn(p.content).length >= t.count) {
        return false
      }
    }
    for ((t) <- matchingRules) {
      if (t.regex.findAllIn(p.content).length < t.count) {
        return false
      }
    }
    return true
  }
}

object MatchingLogic {

  implicit val formats = Serialization.formats(NoTypeHints)

  def parse(raw:String):MatchingLogic = {
    val MLPre = read[MatchingLogicPre](raw)
    return MLPre.get()
  }

  def isValid(ml:MatchingLogic):Boolean = {
      return !(ml.id == null || ml.id == null || ml.id == "" || ml.matchingRules == null || ml.matchingRules.length == 0 || ml.excludingRules == null)
  }
}