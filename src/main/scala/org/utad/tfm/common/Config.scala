package org.utad.tfm.common

import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization
import org.json4s.native.Serialization.read

import scala.io

case class Config(
                   kafkaBootstrapServer:String = "localhost:9092",
                   topicPasties:String = "pasties",
                   topicNewMatchers:String = "new-matchers",
                   CassandraHostname:String = "localhost",
                   CassandraPort:Int = 9042,
                   CassandraUser:String = "pastiesmonitor",
                   CassandraPass:String = "pastiesmonitor",
                   CassandraKeyspace:String = "pastiesmonitor"
                 )

object Config {
  implicit val formats = Serialization.formats(NoTypeHints)

  val file = "config.json"

  def get():Config = {
    val currentDir = System.getProperty("user.dir")
    val configJSON = io.Source.fromFile(currentDir+"/"+file).mkString
    return read[Config](configJSON)
  }
}