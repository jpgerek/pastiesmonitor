package org.utad.tfm.matcher

import java.util
import java.util.{Properties, UUID}

import com.datastax.driver.core.{Cluster, Session}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization._
import org.apache.kafka.streams.kstream.KStreamBuilder
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import org.json4s.NoTypeHints
import org.json4s.native.Serialization
import org.utad.tfm.common.{Config, MatchingLogic, Pastie}

import scala.collection.mutable
import scala.language.implicitConversions
import scala.util.Try

object Matcher {
  implicit val formats = Serialization.formats(NoTypeHints)

  val conf = Config.get()

  println("Config loaded:")
  println(conf)

  val APP_ID = "matcher"

  def getCasSession():Session =
  {
    return new Cluster
      .Builder()
      .addContactPoints(conf.CassandraHostname)
      .withPort(conf.CassandraPort)
      .build()
      .connect(conf.CassandraKeyspace)
  }

  var MatchingLogicList = mutable.HashMap[String, MatchingLogic]()

  private def addMatcher(ml:MatchingLogic) = {
    println("Matching logic added: " + ml.toString)
    MatchingLogicList += (ml.id -> ml)
  }

  def searchMatches() = {
    val casSession = getCasSession()
    val streamConf = new Properties
    streamConf.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, conf.kafkaBootstrapServer)
    streamConf.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    streamConf.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    streamConf.put(StreamsConfig.APPLICATION_ID_CONFIG, APP_ID+"-"+conf.topicPasties)
    streamConf.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    streamConf.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE)

    val builder = new KStreamBuilder
    builder
      .stream(conf.topicPasties)
      .foreach((_:String, v:String) => {
        val pastieTry = Try(Pastie.parse(v))
        if (pastieTry.isFailure) {
          System.err.println("Error parsing pastie: " + v)
        } else {
          val pastie = pastieTry.get
          var result = new util.ArrayList[MatchingLogic]
          print(".")
          MatchingLogicList.foreach(ml => {
            for ((_, ml) <- MatchingLogicList) {
              if (ml.matches(pastie)) {
                println("m")
                result.add(ml)
              }
            }
          })
          result.forEach(ml => {
            casSession.execute(
              """
            INSERT INTO matches(id_matcher_pk, id_match_ck, crawler, pastie_id, pastie_url) VALUES(?, now(), ?, ?, ?)
          """.stripMargin, UUID.fromString(ml.id), pastie.name, pastie.id, pastie.url)
          })
        }
      })

      new KafkaStreams(builder, streamConf).start()
  }

  def listenToNewMatchers() = {
    val streamsConf = new Properties
    streamsConf.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, conf.kafkaBootstrapServer)
    streamsConf.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    streamsConf.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    streamsConf.put(StreamsConfig.APPLICATION_ID_CONFIG, APP_ID+"-"+conf.topicNewMatchers)
    streamsConf.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    streamsConf.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE)

    val builder = new KStreamBuilder
    builder.stream(conf.topicNewMatchers)
      .foreach((k:String, v:String) => {
        val ml = Try(MatchingLogic.parse(v))
        if (ml.isSuccess && MatchingLogic.isValid(ml.get)) {
          addMatcher(ml.get)
        } else {
          println("error in the matcher received", ml.get)
        }
      })

    new KafkaStreams(builder, streamsConf).start()
  }

  def main(args: Array[String]) {
    println("Listening to new data to match")
    searchMatches()

    println("Listening to new matchers")
    listenToNewMatchers()
  }
}

