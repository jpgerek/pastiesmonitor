CREATE KEYSPACE IF NOT EXISTS pastiesmonitor
WITH REPLICATION = {
	'class': 'SimpleStrategy',
	'replication_factor' : 1
};

use pastiesmonitor;

CREATE TABLE IF NOT EXISTS matchers(
  id_pk timeuuid,
  matching_rules list<frozen<tuple<text, int>>>, -- regex, occurrences
  excluding_rules list<frozen<tuple<text, int>>>,-- regex, occurrences
  PRIMARY KEY(id_pk)
);

CREATE TABLE IF NOT EXISTS matches(
  id_matcher_pk timeuuid,
  id_match_ck timeuuid,
  crawler text,
  pastie_id text,
  pastie_url text,
  PRIMARY KEY(id_matcher_pk, id_match_ck)
);